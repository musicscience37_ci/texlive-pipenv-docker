#!/bin/bash

set -e

cd $(dirname $0)

echo "> python version"
python --version

echo "> other python versions"
python3.9 --version
python3.10 --version
python3.11 --version
python3.12 --version

echo "> create Python virtual environment"
pipenv sync --dev

echo "> remove existing file"
latexmk -C

echo "> build PDF"
latexmk
